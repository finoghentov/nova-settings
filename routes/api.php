<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

Route::get('/get-settings', 'SettingController@getSettings');

Route::post('/add-group', 'SettingController@addGroup');

Route::post('/add-new-settings', 'SettingController@addNewSettings');

Route::post('/delete-group', 'SettingController@deleteGroup');

Route::post('/delete-setting', 'SettingController@deleteSetting');

Route::get('/get-languages', 'SettingController@getLanguages');

Route::post('/update-settings', 'SettingController@updateSettingsData');

Route::get('/get-config', 'SettingConfigController@getConfig');

Route::get('/get-config-data', 'SettingConfigController@getConfig');

Route::get('/upload-config', 'SettingConfigController@uploadConfig');

Route::post('/save-config', 'SettingConfigController@saveConfig');

Route::get('/get-editor', 'SettingConfigController@getEditor');

Route::post('/move-setting', 'SettingController@moveSetting');

<?php
if(!function_exists('settings')){
    /**
     * Return value for specify setting
     *
     * @param $key string Key Of Value
     * @param $locale null by default
     * @return string
     */
    function setting($key, $locale = null){

        if(!$locale){
            $locale = app()->getLocale();
        }

        return \Finoghentov\NovaSettings\Models\Setting::getValue($key, $locale);
    }
}

if(!function_exists('settings_group')){
    /**
     * Return group of values for specify group
     *
     * @param $key string Key Of Value
     * @param $locale null by default
     * @return string
     */
    function settings_group($key, $locale = null){

        if(!$locale){
            $locale = app()->getLocale();
        }

        return \Finoghentov\NovaSettings\Models\Setting::getGroupData($key, $locale);
    }
}

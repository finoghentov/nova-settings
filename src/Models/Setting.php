<?php


namespace Finoghentov\NovaSettings\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Setting extends Model
{
    protected $table = 'settings';

    protected $guarded = [];

    /**
     * @param $key string
     * @param $group_id int
     * @return bool
     */
    public static function checkUniqueGroupKey(string $key){
        $group = self::where('group_title', $key)->first();

        if($group){
            return false;
        }

        return true;
    }

    /**
     * @param $key string
     * @param $group_id int
     * @return bool
     */
    public static function checkUniqueKey(string $key, int $group_id){
        $group = self::findOrFail($group_id);

        $dataArray = json_decode($group->settings_data, true);

        if($dataArray){
            foreach($dataArray as $dataKey => $value){
                if($dataKey==$key){
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Return new order for creating field
     * @param $groupId int
     * @param $i int Increment, because in one method we can create a lot of settings
     * @return int
     */
    protected static function getNewOrder(int $groupId, int $i){
        $group = self::find($groupId);
        return count(json_decode($group->settings_data, true))+$i;
    }

    /**
     * @param $groupId int
     * @param $order int
     * @return void
     */
    protected static function sortAfterDelete(int $groupId, int $order){
        $group = self::findOrFail($groupId);
        $data = json_decode($group->settings_data, true);
        foreach($data as $key => $value){
            if($value['order']>$order){
                $data[$key]['order']--;
            }
        }
        $group->settings_data = json_encode($data, JSON_UNESCAPED_UNICODE);
        $group->save();
    }

    /**
     * Return setting value
     *
     * @param $key string
     * @param $locale string
     * @return mixed|string
     */
    public static function getValue($key, $locale){
        $groupKey = stristr($key, '.', true);

        $valueKey = substr($key, strpos($key, '.') + 1, strlen($key));

        try{
            $settingsData = self::group($groupKey)->first()->getSettingsData();
            $item = $settingsData[$valueKey]['value'];

            if(!is_array($item)){
                return $item;
            }

            return $item[$locale];

        }catch(\Exception $exception){
            return $key;
        }
    }

    /**
     * Return setting group data
     *
     * @param $groupKey
     * @param $locale
     * @return Collection
     */
    public static function getGroupData($groupKey, $locale){
        try{
            $settings_data = self::group($groupKey)->first()->getSettingsData();

            $data = collect($settings_data)->map(fn($item) =>
                $item['value'][$locale]
            );

            return $data;
        }catch(\Exception $exception){
            return $groupKey;
        }
    }

    /**
     * Get Settings Data in Object
     *
     * @return mixed
     */
    public function getSettingsData(){
        return json_decode($this->settings_data, true);
    }

    /**
     * Return format of key for setting value
     *
     * @param string $key
     * @return string
     */
    public static function keyFormat(string $key){
        return trim(mb_strtolower(str_replace(' ','_',$key)));
    }

    /**
     * Return format of key for group
     *
     * @param string $name
     * @return string
     */
    private static function groupKeyFormat(string $name){
        return trim(mb_strtolower(str_replace('.','_',str_replace(' ','_',$name))));
    }

    /**
     * Searching by group scope
     *
     * @param $query
     * @param $key
     * @return mixed
     */
    public function scopeGroup($query, $key){
        return $query->where('group_key', $key);
    }

    /**
     * Booting model
     */
    protected static function boot()
    {
        parent::boot();

        static::created(function($group){
            $group->order = self::all()->count();
            $group->group_key = self::groupKeyFormat($group->group_title);
            $group->settings_data = json_encode([],JSON_UNESCAPED_UNICODE);
            $group->save();
        });

        static::addGlobalScope('select', function (Builder $builder) {
            $builder->select('id','group_title','group_key', 'settings_data');
        });

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('order', 'asc');
        });
    }
}

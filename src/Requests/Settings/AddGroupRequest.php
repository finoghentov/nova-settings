<?php

namespace Finoghentov\NovaSettings\Requests\Settings;

use Finoghentov\NovaSettings\Requests\ApiRequest;

class AddGroupRequest extends ApiRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_title' => 'required|string|min:3|max:25'
        ];
    }
}
